/*global require*/
// Node modules
var fs = require('fs'),
    vm = require('vm'),
    merge = require('deeply'),
    chalk = require('chalk'),
    es = require('event-stream');

// Gulp and plugins
var gulp = require('gulp'),
    rjs = require('gulp-requirejs-bundler'),
    concat = require('gulp-concat'),
    clean = require('gulp-clean'),
    replace = require('gulp-replace'),
    rename = require("gulp-rename"),
    uglify = require('gulp-uglify'),
    htmlreplace = require('gulp-html-replace'),
    gutil = require('gulp-util'),
    ftp = require('gulp-ftp'),
    ignore = require('gulp-ignore');

function mergeStringArrays(a, b) {
    var hash = {};
    var ret = [];

    for (var i = 0; i < a.length; i++) {
        var e = a[i];
        if (!hash[e]) {
            hash[e] = true;
            ret.push(e);
        }
    }

    for (var i = 0; i < b.length; i++) {
        var e = b[i];
        if (!hash[e]) {
            hash[e] = true;
            ret.push(e);
        }
    }

    return ret;
}
// Config
var requireJsRuntimeConfig = vm.runInNewContext(fs.readFileSync('Pelicano/app/require.config.js') + '; require;');
requireJsOptimizerConfig = merge(requireJsRuntimeConfig, {
    out: 'scripts.js',
    baseUrl: './Pelicano/',
    name: 'app/startup',
    paths: {
        requireLib: 'Content/requirejs/require'
    },
    include: [
            'requireLib'
        ],
    insertRequire: ['app/startup'],
    bundles: {
        // If you want parts of the site to load on demand, remove them from the 'include' list
        // above, and group them into bundles here.
        // 'bundle-name': [ 'some/module', 'another/module' ],
        // 'another-bundle-name': [ 'yet-another-module' ]
    }
});

// Discovers all AMD dependencies, concatenates together all required .js files, minifies them
gulp.task('js', function () {
    var startup = "./Pelicano/app/startup.js",
        context = {
            jquery: function () {
                return {
                    get: function () {}
                }
            },
            router: {},
            TaskComponentManager: {
                init: function () {}
            },
            ko: {
                validation: {
                    init: function () {}
                },
                components: {
                    components: [],
                    register: function (name, data) {
                        this.components.push(data.require);
                    }
                },
                applyBindings: function () {}
            },
            define: function (reqs, callback) {
                callback(
                    context.jquery,
                    context.ko,
                    context.router,
                    context.TaskComponentManager);
            }
        };
    vm.runInNewContext(fs.readFileSync(startup), context);


    requireJsOptimizerConfig.include = mergeStringArrays(requireJsOptimizerConfig.include, context.ko.components.components);

    return rjs(requireJsOptimizerConfig)
        .pipe(uglify({
            preserveComments: 'some'
        }))
        .pipe(gulp.dest('./build/'));
});

// Concatenates CSS files, rewrites relative paths to Bootstrap fonts, copies Bootstrap fonts
gulp.task('css', function () {
    return gulp.src([
            'Pelicano/Content/components-bootstrap/css/bootstrap.min.css',
            'Pelicano/Content/components-font-awesome/css/font-awesome.min.css',
            'Pelicano/css/styles.css'
        ])
        .pipe(replace(/url\((')?\.\.\/fonts\//g, 'url($1fonts/'))
        .pipe(concat('css.css'))
        .pipe(gulp.dest('./build/css/'));
});

gulp.task('fonts', function () {
    return gulp
        .src([
            'Pelicano/Content/components-bootstrap/fonts/*',
            'Pelicano/Content/components-font-awesome/fonts/*'
        ])
        .pipe(gulp.dest('./build/css/fonts/'));
});

// Copies index.html, replacing <script> and <link> tags to reference production URLs
gulp.task('html', function () {
    return gulp.src('./Pelicano/Views/Home/App.cshtml')
        .pipe(htmlreplace({
            'css': '/css/css.css',
            'js': '/scripts.js'
        }))
        .pipe(gulp.dest('./build/Views/Home/'));
});

gulp.task('clean-extras', function () {
    return gulp.src([
        './build/css/styles.css',
        './build/components',
        './build/Scripts',
        './build/app'
    ], {
            read: false
        })
        .pipe(clean());
});

gulp.task('clean-bin-copy', function () {
    return gulp.src([
            './build/bin/AccesoADatos.dll',
            './build/bin/Pelicano.dll'
        ])
        .pipe(gulp.dest('./build/bin2/'));
});
gulp.task('clean-bin-rename', ['clean-bin-copy'], function () {
    return gulp.src('./build/bin')
        .pipe(clean());
});
gulp.task('clean-bin-devolver', ['clean-bin-rename'], function () {
    return gulp.src('./build/bin2/*')
        .pipe(gulp.dest('./build/bin/'));
});
gulp.task('clean-bin', ['clean-bin-devolver'], function () {
    return gulp.src('./build/bin2')
        .pipe(clean());
});

        

// Removes all files from ./dist/
gulp.task('clean', function () {
    return gulp.src('./build/**/*', {
            read: false
        })
        .pipe(clean());
});

gulp.task('con-dll', ['html', 'clean-extras', 'js', 'css', 'fonts'], function (callback) {
    callback();
    console.log('\nPlaced optimized files in ' + chalk.magenta('build/\n'));
});

gulp.task('default', ['html', 'clean-extras', 'js', 'css', 'fonts', 'clean-bin'], function (callback) {
    callback();
    console.log('\nPlaced optimized files in ' + chalk.magenta('build/\n'));
});

gulp.task('ftp', function () {
    return gulp.src('build/**/*')
        .pipe(ftp({
            host: 'www.pelicano.somee.com',
            user: 'gmora',
            pass: 'Pelicano?2014',
            remotePath: 'www.Pelicano.somee.com'
        }))
        // you need to have some kind of stream after gulp-ftp to make sure it's flushed
        // this can be a gulp plugin, gulp.dest, or any kind of stream
        // here we use a passthrough stream
        .pipe(gutil.noop());
});