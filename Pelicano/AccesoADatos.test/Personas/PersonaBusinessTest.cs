﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AccesoADatos;
using AccesoADatos.Personas;
using AccesoADatos.Propiedades;

namespace AccesoADatos.test
{
    [TestClass]
    public class PersonaBusinessTest
    {
        private PersonaBusiness personaBusiness;

        public PersonaBusinessTest()
        {
            personaBusiness = new PersonaBusiness();
        }

        [TestMethod]
        public void CantidadUsuariosTest()
        {
            var clase = new PropiedadBusiness();
            var resultado = clase.GetCantidadPropiedades();
            Assert.IsTrue(resultado == 0);
        }

        [TestMethod]
        public void AgregarPropietario()
        {
            var contadorPersonas = personaBusiness.GetCantidadPersonas();
            personaBusiness.CrearPropietario("Gerson","Mora", "mi casa", "16.735.880-2","16735880-2");
            var contadorNuevasPersonas = personaBusiness.GetCantidadPersonas();

            Assert.IsTrue(contadorPersonas < contadorNuevasPersonas);

        }


        [TestMethod]
        public void AgregarCliente()
        {
            var contadorPersonas = personaBusiness.GetCantidadPersonas();
            personaBusiness.CrearCliente("Gerson", "Mora", "mi casa", "16.735.880-2", "16735880-2");
            var contadorNuevasPersonas = personaBusiness.GetCantidadPersonas();

            Assert.IsTrue(contadorPersonas < contadorNuevasPersonas);

        }

        [TestMethod]
        public void GertPersonaByID()
        {
            var persona = personaBusiness.GetPersonaByID(1);
            Assert.IsTrue(persona.Nombres.Equals("Gerson"));
        }

        [TestMethod]
        public void LenarClientes()
        {
            personaBusiness.CrearCliente("Gerson", "Mora", "mi casa", "167358802", "gersonmoracid@gmail.com");
            personaBusiness.CrearCliente("Sergio", "Pola", "mi casa", "167358803", "sergio.pola@gmail.com");
            personaBusiness.CrearCliente("Barbara", "Conejeros", "mi casa", "167358804", "baby@gmail.com");
            personaBusiness.CrearCliente("Gloria", "Lara", "mi casa", "167358805", "yoyis@gmail.com");
            Assert.IsTrue(true);
        }

    }
}
