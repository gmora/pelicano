﻿using AccesoADatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoADatos.Personas
{
    public class PersonaBusiness
    {
        private PelicanoDBModelContainer _contexto;
        public PersonaBusiness()
        {
            _contexto = new PelicanoDBModelContainer();
        }

        public Propietario CrearPropietario(string nombres, string apellidoPaterno, string direccion, string rut, string correo)
        {
            var persona = GetPersonaByCorreo(correo);
            if (persona == null)
            {
                persona = CrearPersona(nombres, apellidoPaterno, direccion, rut, correo);
            }

            var propietario = new Propietario()
            {
                FechaIngreso = DateTime.Now,
                Persona = persona,
                Activo = true
            };

            var reuslt = _contexto.PropietarioSet.Add(propietario);
            _contexto.SaveChanges();
            return reuslt;
        }

        public Cliente CrearCliente(string nombres, string apellidoPaterno, string direccion, string rut, string correo)
        {
            var persona = GetPersonaByCorreo(correo);
            if (persona == null)
            {
                persona = CrearPersona(nombres, apellidoPaterno, direccion, rut, correo);
            }

            var arrendatario = new Cliente()
            {
                FechaIngreso = DateTime.Now,
                Persona = persona,
                Activo = true
            };

            var reuslt = _contexto.ClienteSet.Add(arrendatario);
            _contexto.SaveChanges();

            return reuslt;
        }

        private Persona CrearPersona(string nombres, string apellidoPaterno, string direccion, string rut, string correo)
        {
            var persona = new Persona
            {
                Nombres = nombres,
                ApellidoPaterno = apellidoPaterno,
                Direccion = direccion,
                Correo = correo,
                Rut= rut
            };

            return persona;
        }

        /// <summary>
        /// Obtiene una persona por su correo.
        /// </summary>
        /// <param name="correo"></param>
        /// <returns></returns>
        private Persona GetPersonaByCorreo(string correo)
        {
            return _contexto.Personas.Where<Persona>(p => p.Correo == correo).SingleOrDefault();
        }

        public int GetCantidadPersonas()
        {
            var cantPersonas = _contexto.Personas.Count();
            return cantPersonas;
        }

        public Persona GetPersonaByID(int idPersona)
        {
            var persona = _contexto.Personas.Find(idPersona);
            return persona;
        }

        /// <summary>
        /// Retorna un cliente
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        public Cliente GetClienteByRut(string rut)
        {
            return _contexto.ClienteSet.Where<Cliente>(c => c.Persona.Rut == rut).SingleOrDefault<Cliente>();
        }
    }
}
