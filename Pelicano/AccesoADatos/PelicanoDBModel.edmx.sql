
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 12/09/2014 23:23:08
-- Generated from EDMX file: C:\Users\Gerson\Documents\Pelicano\bitbucket\Pelicano\AccesoADatos\PelicanoDBModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [PelicanoDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_PropiedadPropietario]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Propiedades] DROP CONSTRAINT [FK_PropiedadPropietario];
GO
IF OBJECT_ID(N'[dbo].[FK_PropiedadReserva]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Reservas] DROP CONSTRAINT [FK_PropiedadReserva];
GO
IF OBJECT_ID(N'[dbo].[FK_PropietarioPersona]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PropietarioSet] DROP CONSTRAINT [FK_PropietarioPersona];
GO
IF OBJECT_ID(N'[dbo].[FK_PropiedadTipoPropiedad]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Propiedades] DROP CONSTRAINT [FK_PropiedadTipoPropiedad];
GO
IF OBJECT_ID(N'[dbo].[FK_ReservaEstadoReserva]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Reservas] DROP CONSTRAINT [FK_ReservaEstadoReserva];
GO
IF OBJECT_ID(N'[dbo].[FK_ReservaCliente]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ClienteSet] DROP CONSTRAINT [FK_ReservaCliente];
GO
IF OBJECT_ID(N'[dbo].[FK_ClientePersona]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Personas] DROP CONSTRAINT [FK_ClientePersona];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Propiedades]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Propiedades];
GO
IF OBJECT_ID(N'[dbo].[Reservas]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Reservas];
GO
IF OBJECT_ID(N'[dbo].[Personas]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Personas];
GO
IF OBJECT_ID(N'[dbo].[TipoPropiedades]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TipoPropiedades];
GO
IF OBJECT_ID(N'[dbo].[ClienteSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ClienteSet];
GO
IF OBJECT_ID(N'[dbo].[PropietarioSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PropietarioSet];
GO
IF OBJECT_ID(N'[dbo].[EstadoReservaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EstadoReservaSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Propiedades'
CREATE TABLE [dbo].[Propiedades] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Direccion] nvarchar(250)  NOT NULL,
    [Dormitorios] smallint  NOT NULL,
    [Baños] smallint  NOT NULL,
    [Orientacion] nvarchar(10)  NULL,
    [Capacidad] smallint  NOT NULL,
    [Descripcion] nvarchar(max)  NULL,
    [Propietario_Id] int  NOT NULL,
    [TipoPropiedad_Id] int  NOT NULL
);
GO

-- Creating table 'Reservas'
CREATE TABLE [dbo].[Reservas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Fecha_inicio] datetime  NOT NULL,
    [Fecha_fin] datetime  NOT NULL,
    [Valor] decimal(18,0)  NOT NULL,
    [Propiedad_ID] int  NOT NULL,
    [EstadoReserva_Id] int  NOT NULL
);
GO

-- Creating table 'Personas'
CREATE TABLE [dbo].[Personas] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombres] nvarchar(100)  NOT NULL,
    [Direccion] nvarchar(255)  NOT NULL,
    [Rut] nvarchar(15)  NULL,
    [Correo] nvarchar(max)  NOT NULL,
    [ApellidoPaterno] nvarchar(100)  NOT NULL,
    [ApellidoMaterno] nvarchar(100)  NULL,
    [TelefonoFijo] nvarchar(15)  NULL,
    [TelefonoCelular] nvarchar(15)  NULL
);
GO

-- Creating table 'TipoPropiedades'
CREATE TABLE [dbo].[TipoPropiedades] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ClienteSet'
CREATE TABLE [dbo].[ClienteSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FechaIngreso] datetime  NOT NULL,
    [Activo] bit  NOT NULL,
    [ReservaCliente_Cliente_Id] int  NULL,
    [Persona_Id] int  NOT NULL
);
GO

-- Creating table 'PropietarioSet'
CREATE TABLE [dbo].[PropietarioSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FechaIngreso] datetime  NOT NULL,
    [Activo] bit  NOT NULL,
    [Persona_Id] int  NOT NULL
);
GO

-- Creating table 'EstadoReservaSet'
CREATE TABLE [dbo].[EstadoReservaSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'Propiedades'
ALTER TABLE [dbo].[Propiedades]
ADD CONSTRAINT [PK_Propiedades]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [Id] in table 'Reservas'
ALTER TABLE [dbo].[Reservas]
ADD CONSTRAINT [PK_Reservas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Personas'
ALTER TABLE [dbo].[Personas]
ADD CONSTRAINT [PK_Personas]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TipoPropiedades'
ALTER TABLE [dbo].[TipoPropiedades]
ADD CONSTRAINT [PK_TipoPropiedades]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ClienteSet'
ALTER TABLE [dbo].[ClienteSet]
ADD CONSTRAINT [PK_ClienteSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PropietarioSet'
ALTER TABLE [dbo].[PropietarioSet]
ADD CONSTRAINT [PK_PropietarioSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EstadoReservaSet'
ALTER TABLE [dbo].[EstadoReservaSet]
ADD CONSTRAINT [PK_EstadoReservaSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Propietario_Id] in table 'Propiedades'
ALTER TABLE [dbo].[Propiedades]
ADD CONSTRAINT [FK_PropiedadPropietario]
    FOREIGN KEY ([Propietario_Id])
    REFERENCES [dbo].[PropietarioSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PropiedadPropietario'
CREATE INDEX [IX_FK_PropiedadPropietario]
ON [dbo].[Propiedades]
    ([Propietario_Id]);
GO

-- Creating foreign key on [Propiedad_ID] in table 'Reservas'
ALTER TABLE [dbo].[Reservas]
ADD CONSTRAINT [FK_PropiedadReserva]
    FOREIGN KEY ([Propiedad_ID])
    REFERENCES [dbo].[Propiedades]
        ([ID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PropiedadReserva'
CREATE INDEX [IX_FK_PropiedadReserva]
ON [dbo].[Reservas]
    ([Propiedad_ID]);
GO

-- Creating foreign key on [Persona_Id] in table 'PropietarioSet'
ALTER TABLE [dbo].[PropietarioSet]
ADD CONSTRAINT [FK_PropietarioPersona]
    FOREIGN KEY ([Persona_Id])
    REFERENCES [dbo].[Personas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PropietarioPersona'
CREATE INDEX [IX_FK_PropietarioPersona]
ON [dbo].[PropietarioSet]
    ([Persona_Id]);
GO

-- Creating foreign key on [TipoPropiedad_Id] in table 'Propiedades'
ALTER TABLE [dbo].[Propiedades]
ADD CONSTRAINT [FK_PropiedadTipoPropiedad]
    FOREIGN KEY ([TipoPropiedad_Id])
    REFERENCES [dbo].[TipoPropiedades]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PropiedadTipoPropiedad'
CREATE INDEX [IX_FK_PropiedadTipoPropiedad]
ON [dbo].[Propiedades]
    ([TipoPropiedad_Id]);
GO

-- Creating foreign key on [EstadoReserva_Id] in table 'Reservas'
ALTER TABLE [dbo].[Reservas]
ADD CONSTRAINT [FK_ReservaEstadoReserva]
    FOREIGN KEY ([EstadoReserva_Id])
    REFERENCES [dbo].[EstadoReservaSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ReservaEstadoReserva'
CREATE INDEX [IX_FK_ReservaEstadoReserva]
ON [dbo].[Reservas]
    ([EstadoReserva_Id]);
GO

-- Creating foreign key on [ReservaCliente_Cliente_Id] in table 'ClienteSet'
ALTER TABLE [dbo].[ClienteSet]
ADD CONSTRAINT [FK_ReservaCliente]
    FOREIGN KEY ([ReservaCliente_Cliente_Id])
    REFERENCES [dbo].[Reservas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ReservaCliente'
CREATE INDEX [IX_FK_ReservaCliente]
ON [dbo].[ClienteSet]
    ([ReservaCliente_Cliente_Id]);
GO

-- Creating foreign key on [Persona_Id] in table 'ClienteSet'
ALTER TABLE [dbo].[ClienteSet]
ADD CONSTRAINT [FK_ClientePersona]
    FOREIGN KEY ([Persona_Id])
    REFERENCES [dbo].[Personas]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ClientePersona'
CREATE INDEX [IX_FK_ClientePersona]
ON [dbo].[ClienteSet]
    ([Persona_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------