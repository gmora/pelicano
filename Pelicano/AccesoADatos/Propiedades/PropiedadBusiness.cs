﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccesoADatos.Propiedades
{
    public class PropiedadBusiness
    {
        public PropiedadBusiness()
        {
        }

        public int GetCantidadPropiedades()
        {
            PelicanoDBModelContainer contexto = new PelicanoDBModelContainer();

            var cantPropiedades = contexto.Propiedades.Count();
            return cantPropiedades;
        }
    }
}
