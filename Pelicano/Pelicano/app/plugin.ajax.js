﻿/*global define*/
define(['jquery'], function ($) {
    'use strict';

    function Ajax() {
        this.get = function (url, data) {
            return $.ajax({
                url: url,
                dataType: 'json',
                type: 'GET',
                data: data
            });
        };
        this.post = function (url, data) {
            //TODO: Falta el content type json
            return $.ajax({
                url: url,
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                type: 'POST',
                data: JSON.stringify(data)
            });
        };
    }

    return new Ajax();
});