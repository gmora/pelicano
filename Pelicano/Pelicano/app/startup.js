/*global define*/
define(['jquery', 'knockout', 'router', 'bootstrap', 'knockout-projections', './knockout-extends'], function ($, ko, router) {
    'use strict';

    // Components can be packaged as AMD modules, such as the following:
    ko.components.register('home-page', {
        require: 'components/home-page/home'
    });

    // ... or for template-only components, you can just point to a .html file directly:
    ko.components.register('about-page', {
        template: {
            require: 'text!components/about-page/about.html'
        }
    });

    /************
     * Tags
     ***********/

    ko.components.register('header-bar', {
        require: 'components/tags/header-bar/header-bar'
    });
    ko.components.register('header-page', {
        require: 'components/tags/header-page/header-page'
    });
    ko.components.register('nav-bar', {
        require: 'components/tags/nav-bar/nav-bar'
    });
    ko.components.register('propiedad-resumen', {
        require: 'components/tags/propiedades/resumen/propiedad-resumen'
    });

    /************
     * P�ginas
     ***********/

    ko.components.register('buscar-page', {
        require: 'components/pages/buscar/buscar-page/buscar-page'
    });
    ko.components.register('buscar-resultados', {
        require: 'components/pages/buscar/buscar-page/buscar-resultados'
    });
    ko.components.register('propiedades/ingresar-page', {
        require: 'components/pages/propiedades/ingresar/ingresar-page'
    });
    ko.components.register('clientes/ingresar-page', {
        require: 'components/pages/clientes/ingresar/ingresar-page'
    });

    // [Scaffolded component registrations will be inserted here. To retain this feature, don't remove this comment.]

    // Start the application
    ko.applyBindings({
        route: router.currentRoute
    });
});