/*global define*/
define(["knockout", "crossroads", "hasher"], function (ko, crossroads, hasher) {
    'use strict';

    // This module configures crossroads.js, a routing library. If you prefer, you
    // can use any other routing library (or none at all) as Knockout is designed to
    // compose cleanly with external libraries.
    //
    // You *don't* have to follow the pattern established here (each route entry
    // specifies a 'page', which is a Knockout component) - there's nothing built into
    // Knockout that requires or even knows about this technique. It's just one of
    // many possible ways of setting up client-side routes.

    function activateCrossroads() {
        function parseHash(newHash, oldHash) {
            crossroads.parse(newHash);
        }
        crossroads.normalizeFn = crossroads.NORM_AS_OBJECT;
        hasher.initialized.add(parseHash);
        hasher.changed.add(parseHash);
        hasher.init();
    }

    function Router(config) {
        var self = this,
            currentRoute = self.currentRoute = ko.observable({}),
            heading = self.heading = {
                title: ko.observable(),
                subtitle: ko.observable(),
                set: function (title, subtitle) {
                    this.title(title || "");
                    this.subtitle(subtitle || "");
                }
            };

        ko.utils.arrayForEach(config.routes, function (route) {
            crossroads.addRoute(route.url, function (requestParams) {
                var params = ko.utils.extend(requestParams, route.params);
                params = ko.utils.extend(params, {
                    data: self.params,
                    heading: heading
                });

                heading.set("", "");
                self.params = null;
                currentRoute(params);
            });
        });

        activateCrossroads();

        this.push = function (url, params) {
            this.params = params;
            hasher.setHash(url);
        };
    }

    return new Router({
        routes: [
            {
                url: 'home',
                params: {
                    page: 'home-page',
                    group: 'home'
                }
            }, {
                url: '',
                params: {
                    page: 'home-page',
                    group: 'home'
                }
            }, {
                url: 'buscar',
                params: {
                    page: 'buscar-page',
                    group: 'buscar'
                }
            }, {
                url: 'buscar/resultados',
                params: {
                    page: 'buscar-resultados',
                    group: 'buscar'
                }
            }, {
                url: 'propiedades/ingresar',
                params: {
                    page: 'propiedades/ingresar-page',
                    group: 'propiedad-ingresar'
                }
            }, {
                url: 'clientes/ingresar',
                params: {
                    page: 'clientes/ingresar-page',
                    group: 'clientes-ingresar'
                }
            }
        ]
    });
});