// require.js looks for the following global when initializing
var require = {
    baseUrl: "/",
    paths: {
        "bootstrap": "Content/components-bootstrap/js/bootstrap.min",
        "bootstrap-datepicker": "Content/bootstrap-datepicker/js/locales/bootstrap-datepicker.es",
        "crossroads": "Content/crossroads/dist/crossroads",
        "hasher": "Content/hasher/dist/js/hasher",
        "jquery": "Content/jquery/dist/jquery",
        "autoNumeric": "Content/autoNumeric/autoNumeric",
        "knockout": "Content/knockout/dist/knockout",
        "knockout-projections": "Content/knockout-projections/dist/knockout-projections",
        "signals": "Content/js-signals/dist/signals",
        "text": "Content/requirejs-text/text",
        "router": "app/router",
        "Ajax": "app/plugin.ajax",
        "models": "components/models"
    },
    shim: {
        "bootstrap": {
            deps: ["jquery"]
        },
        "bootstrap-datepicker": {
            deps: ["jquery", "Content/bootstrap-datepicker/js/bootstrap-datepicker"]
        },
        "autoNumeric": ['jquery']
    },
    deps: ["jquery", "app/startup"]
};