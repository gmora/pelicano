/*global define*/
define([
    'knockout',
    'jquery',
    'bootstrap-datepicker',
    'autoNumeric',
    'Content/jquery-meiomask/dist/meiomask'
], function (ko, $) {

    ko.bindingHandlers.datepicker = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var value = valueAccessor(),
                allBindings = allBindingsAccessor(),
                $element = $(element);

            $element.datepicker({
                format: 'dd/mm/yyyy',
                weekStart: 1,
                date: allBindings.value()
            }).on('changeDate', function (ev) {
                allBindings.value(ev.date);
                $element.datepicker('hide');
            });
            //$element.datepicker('setValue', allBindings.value());

        },
        update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
            var value = valueAccessor();
            $(element).datepicker('setValue', value);
        }
    };

    ko.bindingHandlers.modal = {
        init: function (element, valueAccessor) {
            $(element).modal({
                show: false
            });

            var value = valueAccessor();
            if (typeof value === 'function') {
                $(element).on('hide.bs.modal', function () {
                    value(false);
                });
            }
            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $(element).remove();
            });

        },
        update: function (element, valueAccessor) {
            var value = valueAccessor();
            if (ko.utils.unwrapObservable(value)) {
                $(element).modal('show');
            } else {
                $(element).modal('hide');
            }
        }
    };

    ko.bindingHandlers.currency = {
        init: function (element, valueAccessor, allBindingsAccessor) {

            var value = valueAccessor(),
                allBindings = allBindingsAccessor(),
                currency = allBindings.currency,
                $element = $(element);

            $element.autoNumeric('init', {
                aSep: '.',
                dGroup: '3',
                aDec: ',',
                aSign: '$',
                mDec: '0'
            });
            
            if (!ko.isObservable(allBindings.text)) {
                return;
            }

            var text = allBindings.text,
                subs = text.subscribe(function () {
                    $element.autoNumeric('update', {});
                });

            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                subs.dispose();
            });
            
        }
    };

    $.mask.rules.k = /[kK0-9]/;
    ko.bindingHandlers.mask = {
        init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var $element = $(element),
                allBindings = allBindingsAccessor();

            $element.setMask({
                mask: allBindings.mask,
                type: 'reverse',
                autoTab: false
            });
        }
    };
});