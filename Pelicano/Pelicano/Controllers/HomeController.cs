﻿using AccesoADatos.Personas;
using Pelicano.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Pelicano.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult App()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Persona()
        {            
            var personaBuisiness = new  PersonaBusiness();
            var persona = personaBuisiness.GetPersonaByID(1);            
            var personaModel = new PersonaModel();
            personaModel.Nombre = persona.Nombres;
            personaModel.Direccion = persona.Direccion;
            return View(personaModel);
        }
    }
}