﻿using AccesoADatos.Personas;
using Pelicano.Models.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Pelicano.Controllers.Api
{
    public class ClienteFilterViewModel
    {
        public string rut { get; set; }
    }

    public class ClientesController : ApiController
    {
        // GET: Cliente
        public ClienteDTO Get([FromUri] ClienteFilterViewModel filter)
        {
            var clienteBusiness = new PersonaBusiness();

            var cliente = clienteBusiness.GetClienteByRut(filter.rut);
            if (cliente != null)
            {
                return new ClienteDTO()
                {
                    Id = cliente.Id,
                    Rut = cliente.Persona.Rut,
                    Nombres = cliente.Persona.Nombres,
                    App = cliente.Persona.ApellidoPaterno,
                    Apm = cliente.Persona.ApellidoMaterno,
                    Telefono = cliente.Persona.TelefonoFijo,
                    Celular = cliente.Persona.TelefonoCelular,
                    Direccion = cliente.Persona.Direccion,
                    Email = cliente.Persona.Correo
                };
            }
            else
            {
                return new ClienteDTO()
                {
                    Rut = filter.rut
                };
            }
        }

        public long Post(ClienteDTO cliente)
        {
            //TODO: Ver si devolver el dto el puro id. GERSON!!!!
            return 10;
        }

    }
}
