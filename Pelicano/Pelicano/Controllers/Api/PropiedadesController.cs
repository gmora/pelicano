﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Pelicano.Controllers.Api
{
    public class PropiedadResultadoDTO
    {
        public long codigo { get; set; }
        public int capacidad { get; set; }
        public string orientacion { get; set; }
        public int valor { get; set; }
        public int valorPeriodo
        {
            get
            {
                //TODO: Esto está en duro para efectos de prueba
                return this.duracion * this.valor;
            }
            set { }
        }
        public int duracion { get; set; }
    }

    public class PropiedadesFilterViewModel
    {
        public string desde { get; set; }
        public string hasta { get; set; }
        public int capacidad { get; set; }
    }

    public class PropiedadesController : ApiController
    {
        public IList<PropiedadResultadoDTO> Get([FromUri]PropiedadesFilterViewModel filter)
        {
            //DateTime desde = DateTime.ParseExact(filter.desde + " 00:00", "dd/MM/yyyy hh:mm", CultureInfo.InvariantCulture);
            //DateTime hasta = DateTime.ParseExact(filter.hasta + " 23:59", "dd/MM/yyyy hh:mm", CultureInfo.InvariantCulture);
            DateTime desde = Convert.ToDateTime(filter.desde + " 00:00");
            DateTime hasta = Convert.ToDateTime(filter.hasta + " 23:59");

            int periodo = (int)Math.Ceiling((hasta.Date - desde.Date).TotalDays);

            return new List<PropiedadResultadoDTO>() { 
                new PropiedadResultadoDTO{codigo= 1, capacidad=filter.capacidad, orientacion="N", valor=50000, duracion = periodo},
                new PropiedadResultadoDTO{codigo= 2, capacidad=filter.capacidad+5, orientacion="O", valor=40000, duracion = periodo},
                new PropiedadResultadoDTO{codigo= 3, capacidad=filter.capacidad+2, orientacion="E", valor=60000, duracion = periodo},
            
            };
        }
    }
}