﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pelicano.Startup))]
namespace Pelicano
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
