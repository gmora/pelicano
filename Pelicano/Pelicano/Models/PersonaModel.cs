﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelicano.Models
{
    public class PersonaModel
    {
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
    }
}