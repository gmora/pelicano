﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Pelicano.Models.Cliente
{
    public class ClienteDTO
    {
        public long Id { get; set; }
        public string Rut { get; set; }
        public string Nombres { get; set; }
        public string App { get; set; }
        public string Apm { get; set; }
        public string Telefono { get; set; }
        public string Celular { get; set; }
        public string Direccion { get; set; }
        public string Email { get; set; }
    }
}