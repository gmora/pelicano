/*global define*/
define(['knockout', 'text!./buscar-resultados.html', 'Ajax', 'models/Propiedad'], function (ko, templateMarkup, Ajax, Propiedad) {
    'use strict';

    function BuscarPage(params) {

        if (!params.data) {
            window.location.href = "#/buscar";
            return;
        }

        params.heading.set("B�squeda propiedades", "resultados");

        var self = this,
            data = params.data || {};
        this.params = data;
        this.desde = ko.observable(data.desde);
        this.hasta = ko.observable(data.hasta);
        this.capacidad = ko.observable(data.capacidad);

        this.propiedadesRaw = ko.observableArray([]);

        this.propiedades = ko.computed(function () {
            return this.propiedadesRaw();
        }, this);

        this.__propiedad = ko.observable(new Propiedad({}));
        this.propiedad = ko.observable(new Propiedad({}));/*ko.pureComputed({
            read: function () {
                return this.__propiedad();
            },
            write: function (value) {
                this.__propiedad().load(value);
            },
            owner: this
        });*/

        self.showDetalle = ko.observable(false);

        self.detalle = function (propiedad) {
            self.propiedad(propiedad);
            self.showDetalle(true);
        };

        this.load();
    }

    // This runs when the component is torn down. Put here any logic necessary to clean up,
    // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
    BuscarPage.prototype.dispose = function () { };

    BuscarPage.prototype.load = function () {
        var self = this;
        Ajax.get('/rest/Propiedades', self.params)
            .done(function (data) {
                self.propiedadesRaw(ko.utils.arrayMap(data, function (pData) {
                    var p = new Propiedad(pData);
                    p.marcado = ko.observable(false);
                    return p;
                }));
            });
    };

    BuscarPage.prototype.addEmail = function () {
        this.propiedad().marcado(true);
    };

    return {
        viewModel: BuscarPage,
        template: templateMarkup
    };

});