/*global define, require*/
define(['knockout', 'text!./buscar-page.html'], function (ko, templateMarkup) {
    'use strict';

    function BuscarPage(params) {
        params.heading.set("Búsqueda propiedades");

        var self = this,
            today = new Date(),
            oneWeek = new Date(today.getTime() + (7 * 24 * 60 * 60 * 1000));

        //TODO: Ojo acá, lo del local string podría dejar la escoba
        self.desde = ko.observable(today.toLocaleDateString());
        self.hasta = ko.observable(oneWeek.toLocaleDateString());
        self.capacidad = ko.observable();

        self.codigo = ko.observable();
    }

    // This runs when the component is torn down. Put here any logic necessary to clean up,
    // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
    BuscarPage.prototype.dispose = function () {};
    BuscarPage.prototype.buscar = function () {
        require("router").push("buscar/resultados", {
            desde: this.desde(),
            hasta: this.hasta(),
            capacidad: this.capacidad()
        });
    };

    BuscarPage.prototype.buscarCodigo = function () {
        /*
        require("router").push("buscar/resultados", {
            desde: this.desde(),
            hasta: this.hasta(),
            capacidad: this.capacidad()
        });
        */
        alert("Estamos trabajando para usted");
    };

    return {
        viewModel: BuscarPage,
        template: templateMarkup
    };

});