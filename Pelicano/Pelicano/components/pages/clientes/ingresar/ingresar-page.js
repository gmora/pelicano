/*global define, require*/
define(['knockout', 'text!./ingresar-page.html', 'Ajax', 'models/Cliente'], function (ko, templateMarkup, Ajax, Cliente) {
    'use strict';

    function ViewModel(params) {
        params.heading.set("Clientes");

        var self = this;

        self.cliente = new Cliente({});
        self.mostrarAlert = ko.observable(false);
        self.mostrarDetalle = ko.observable(false);
        self.mostrarInfoCreado = ko.observable(false);
    }

    ViewModel.prototype.dispose = function () {};
    ViewModel.prototype.buscarCliente = function () {
        var self = this,
            cliente = self.cliente,
            rut = cliente.rut().replace(/\./g, '').replace('-', '');

        self.mostrarAlert(false);
        self.mostrarInfoCreado(false);

        Ajax.get('/rest/clientes', {
            rut: rut
        })
            .done(function (data) {
                if (!!data && !!data.Id) {
                    cliente.load(data);
                } else {
                    self.mostrarAlert(true);
                    cliente.load({
                        Rut: rut
                    });
                }
                self.mostrarDetalle(true);
            });
    };
    ViewModel.prototype.guardar = function () {
        var self = this,
            cliente = self.cliente;
        Ajax.post('/rest/clientes', cliente.toJS())
            .done(function (data) {
                cliente.id(data);
                self.mostrarAlert(false);
                self.mostrarInfoCreado(true);
            });
    };
    ViewModel.prototype.cancelar = function () {};

    return {
        viewModel: ViewModel,
        template: templateMarkup
    };

});