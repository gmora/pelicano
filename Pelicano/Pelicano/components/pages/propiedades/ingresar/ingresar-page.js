/*global define, require*/
define(['knockout', 'text!./ingresar-page.html'], function (ko, templateMarkup) {
    'use strict';

    function Cliente(params) {
        var self = this;

        self.id = ko.observable(params.id);
        self.rut = ko.observable(params.rut);
    }

    function ViewModel(params) {
        params.heading.set("Ingresar propiedad");

        var self = this;

        self.cliente = new Cliente({});
        self.mostrarAlert = ko.observable(false);
    }

    ViewModel.prototype.dispose = function () { };
    ViewModel.prototype.buscarCliente = function () {
        //TODO: Acá hay que ir a buscar cosas a alguna parte
        this.mostrarAlert( true);
    };

    return {
        viewModel: ViewModel,
        template: templateMarkup
    };

});