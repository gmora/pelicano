/*global define*/
define(['knockout'], function (ko) {
    'use strict';

    function Cliente(params) {
        var self = this;

        self.id = ko.observable(params.id);
        self.rut = ko.observable(params.rut);
        self.nombres = ko.observable(params.nombres);
        self.app = ko.observable(params.app);
        self.apm = ko.observable(params.apm);
        self.telefono = ko.observable(params.telefono);
        self.celular = ko.observable(params.celular);
        self.direccion = ko.observable(params.direccion);
        self.email = ko.observable(params.email);
    }
    Cliente.prototype.toJS = function () {
        var self = this;
        return {
            id: self.id(),
            rut: self.rut(),
            nombres: self.nombres(),
            app: self.app(),
            apm: self.apm(),
            telefono: self.telefono(),
            celular: self.celular(),
            direccion: self.direccion(),
            email: self.email()
        };
    };
    Cliente.prototype.load = function (data) {
        var self = this;

        self.id(data.Id);
        self.rut(data.Rut);
        self.nombres(data.Nombres);
        self.app(data.App);
        self.apm(data.Apm);
        self.telefono(data.Telefono);
        self.celular(data.Celular);
        self.direccion(data.Direccion);
        self.email(data.Email);
    };

    return Cliente;
});