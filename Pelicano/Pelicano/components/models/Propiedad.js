﻿/*global define*/
define(['knockout'], function (ko) {
    'use strict';

    var Propiedad = function (params) {
        var self = this;

        self.codigo = ko.observable(params.codigo);
        self.capacidad = ko.observable(params.capacidad);
        self.orientacion = ko.observable(params.orientacion);
        self.valor = ko.observable(params.valor);
        self.valorPeriodo = ko.observable(params.valorPeriodo);
    };

    Propiedad.prototype.load = function (params) {
        var self = this;

        self.codigo(params.codigo);
        self.capacidad(params.capacidad);
        self.orientacion(params.orientacion);
        self.valor(params.valor);
        self.valorPeriodo(params.valorPeriodo);
    };

    return Propiedad;

});