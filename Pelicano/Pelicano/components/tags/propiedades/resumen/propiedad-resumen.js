﻿/*global define*/
define(['knockout', 'text!./propiedad-resumen.html'], function (ko, template) {

    var ViewModel = function (params) {
        this.propiedad = params.propiedad;
    };


    return {
        viewModel: ViewModel,
        template: template
    };
});