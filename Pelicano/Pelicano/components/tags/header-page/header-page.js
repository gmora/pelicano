﻿/*global define*/
define(['knockout', 'text!./header-page.html'], function (ko, template) {
    'use strict';

    function ViewModel(params) {

        this.title = params.title;
        this.subtitle = params.subtitle;
    }

    return {
        viewModel: ViewModel,
        template: template
    };
});